<?php
ob_start();
// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
    wp_enqueue_style('ultimate icons',get_stylesheet_directory_uri()."/fonts/ultimate-icons/style.css","","",1);
});

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );


// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );


 
// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');


function register_shortcodes(){
  $dir=dirname(__FILE__)."/shortcodes";
  $files=scandir($dir);
  foreach($files as $k=>$v){
    $file=$dir."/".$v;
    if(strstr($file,".php")){
      $shortcode=substr($v,0,-4);
      add_shortcode($shortcode,function($attr,$content,$tag){
        ob_start();
        include(dirname(__FILE__)."/shortcodes/".$tag.".php");
        $c=ob_get_clean();
        return $c;
      });
    }
  }
}



// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');



if (@$_GET['keyword'] != '' && @$_GET['brand'] !="") {
    $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $url = explode('?', $url);
    setcookie('keyword', $_GET['keyword']);
    setcookie('brand', $_GET['brand']);
    wp_redirect($url[0]);
    exit;
} elseif (@$_GET['brand'] !="" && @$_GET['keyword'] == '') {
    $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $url = explode('?', $url);
    setcookie('brand', $_GET['brand']);
    wp_redirect($url[0]);
    exit;
} elseif (@$_GET['brand'] =="" && @$_GET['keyword'] != '') {
    $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $url = explode('?', $url);
    setcookie('keyword', $_GET['keyword']);
    wp_redirect($url[0]);
    exit;
}
 
 
// shortcode to show H1 google keyword fields
function new_google_keyword()
{
    if (@$_COOKIE['keyword'] ==""  && @$_COOKIE['brand'] == "") {
        // return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on Flooring<h1>';
        return $google_keyword = '<h1 class="googlekeyword">Save up to $1000 on Flooring<h1>';
    } else {
        $keyword = $_COOKIE['keyword'];
        $brand = $_COOKIE['brand'];
        return $google_keyword = '<h1 class="googlekeyword">Save up to $1000 on '.$brand.' '.$keyword.'<h1>';
    }
}
  add_shortcode('google_keyword_code', 'new_google_keyword');

function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

function featured_products_fun() {
	$args = array(
        'post_type' => array('carpeting', 'luxury_vinyl_tile', 'solid_wpc_waterproof', 'hardwood_catalog', 'laminate_catalog', 'tile_catalog'),
        'post__not_in' => array(323965,302354), 
        'posts_per_page' => 8,
        'orderby' => 'rand',
        'meta_query' => array(
            array(
                'key' => 'featured', 
                'value' => true, 
                'compare' => '=='
            )
        ) 
    );
    $query = new WP_Query( $args );
    if( $query->have_posts() ){
        $outpout = '<div class="featured-products"><div class="featured-product-list">';
        while ($query->have_posts()) : $query->the_post(); 
            $gallery_images = get_field('gallery_room_images');
            $gallery_img = explode("|",$gallery_images);
            $count = 0;
            // loop through the rows of data
            foreach($gallery_img as  $key=>$value) {
                $room_image = $value;
                if(!$room_image){
                    $room_image = "http://placehold.it/245x310?text=No+Image";
                }
                else{
                    if(strpos($room_image , 's7.shawimg.com') !== false){
                        if(strpos($room_image , 'http') === false){ 
                            $room_image = "http://" . $room_image;
                        }
                        $room_image = $room_image ;
                    } else{
                        if(strpos($room_image , 'http') === false){ 
                            $room_image = "https://" . $room_image;
                        }
                        $room_image= "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[524x636]&sink";
                    }
                }
                if($count>0){
                    break;
                }
                $count++;
            }

            $color_name = get_field('color');
            $post_type = get_post_type_object(get_post_type( get_the_ID() ));
            
            $outpout .= '<div class="featured-product-item">
                <div class="featured-inner">
                    <div class="prod-img-wrap">
                        <img src="'.$room_image.'" alt="'.get_the_title().'" />
                        <div class="button-wrapper">
                            <div class="button-wrapper-inner">
                                <a href="'.get_the_permalink().'" class="button alt">View Product</a>
                                <a href="/flooring-coupon/" class="button">Get Coupon</a>
                            </div>
                        </div>
                    </div>
                    <div class="product-info">
                        <h5><a href="'.get_the_permalink().'">'.$color_name.'</a></h5>
                        <h6>'.$post_type->labels->singular_name.'</h6>
                    </div>
                </div>
            </div>';

        endwhile;
        $outpout .= '</div></div>';
        wp_reset_query();
    }  


    return $outpout;
}
add_shortcode( 'featured_products', 'featured_products_fun' );

function filter_site_upload_size_limit( $size ) {
      $size = 512 * 1024 * 1024;
   
    return $size;
}
add_filter( 'upload_size_limit', 'filter_site_upload_size_limit', 20 );


wp_clear_scheduled_hook( '404_redirection_301_log_cronjob' );

if (!wp_next_scheduled('maccos_404_redirection_301_log_cronjob')) {    
   
    wp_schedule_event( time() +  17800, 'daily', 'maccos_404_redirection_301_log_cronjob');
}

add_action( 'maccos_404_redirection_301_log_cronjob', 'maccos_custom_404_redirect_hook' ); 



function check_404_maccos($url) {
   $headers=get_headers($url, 1);
   if ($headers[0]!='HTTP/1.1 200 OK') {return true; }else{ return false;}
}

 // custom 301 redirects from  404 logs table
 function maccos_custom_404_redirect_hook(){
    global $wpdb;    
    write_log('in function');

    $table_redirect = $wpdb->prefix.'redirection_items';
    $table_name = $wpdb->prefix . "redirection_404";
    $table_group = $wpdb->prefix.'redirection_groups';

     $data_404 = $wpdb->get_results( "SELECT * FROM $table_name" );
     $datum = $wpdb->get_results("SELECT * FROM $table_group WHERE name = 'Products'");
     $redirect_group =  $datum[0]->id;  

    if ($data_404)
    {      
      foreach ($data_404 as $row_404) 
       {            

        if (strpos($row_404->url,'carpet') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {
            write_log($row_404->url);      
            
           $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = check_404_maccos($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

           
            $destination_url_carpet = '/flooring-products/carpeting/carpeting-catalog/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_carpet,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            write_log( 'carpet 301 added ');
         }else{

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');


         }

        }
        else if (strpos($row_404->url,'hardwood') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url);

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');
            $url_404 = home_url().''.$row_404->url;
            $headers_res = check_404_maccos($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            
            $destination_url_hardwood = '/flooring-products/hardwood-flooring/hardwood-catalog/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_hardwood,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

           }else{

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

           }

            write_log( 'hardwood 301 added ');

        }
        else if (strpos($row_404->url,'laminate') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url); 

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = check_404_maccos($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            
            $destination_url_laminate = '/flooring-products/laminate-flooring/laminate-catalog/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" =>$row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_laminate,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }

            write_log( 'laminate 301 added ');


        }
        else if ((strpos($row_404->url,'luxury-vinyl') !== false || strpos($row_404->url,'vinyl') !== false) && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url);  

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = check_404_maccos($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            
            if (strpos($row_404->url,'luxury-vinyl') !== false ){
                $destination_url_lvt = '/flooring-products/luxury-vinyl-tile/vinyl-catalog/';
            }elseif(strpos($row_404->url,'vinyl') !== false){
                $destination_url_lvt = '/flooring-products/luxury-vinyl-tile/vinyl-catalog/';
            }
            
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" =>$row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_lvt,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
            }

            write_log( 'luxury-vinyl 301 added ');
        }
        else if (strpos($row_404->url,'tile') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url); 

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = check_404_maccos($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $destination_url_tile = '/flooring-products/tile/tile-catalog/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_tile,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }

            write_log( 'tile 301 added ');
        }

       }  
    }

 }
 

 function custom_plugin_register_settings() {

    register_setting('custom_plugin_options_group', 'first_field_name');
   
}

add_action('admin_init', 'custom_plugin_register_settings');

function custom_plugin_setting_page() {

  // add_options_page( string $page_title, string $menu_title, string $capability, string $menu_slug, callable $function = '' )
   add_options_page('Custom Plugin', 'Job Listing Export Setting', 'manage_options', 'export_joblisting_data', 'custom_page_html_form');
   // custom_page_html_form is the function in which I have written the HTML for my custom plugin form.
}

add_action('admin_menu', 'custom_plugin_setting_page');

function custom_page_html_form() { ?>
    <div class="wrap">
        <h2>Job Listing Export Setting</h2>
        <form method="post" action="/wp-admin/admin.php?page=export_joblisting_data">
            <?php settings_fields('custom_plugin_options_group'); ?>

            <table class="form-table" width>
                <tr>
                    <th><label for="first_field_id">Click to Below button to Export:</label></th>

                </tr>
                       
            </table>
            <input type = 'hidden' class="regular-text" id="first_field_id" name="export_data" value="export_data">
            <?php submit_button('Export to CSV'); ?>
    </div>
<?php } ?>

<?php 
       if(isset($_POST['export_data'] )){

      global  $wpdb;

      $sql_data = "SELECT p.post_title, 
      m1.meta_value as 'job_applicant_name',
      m2.meta_value as 'email', 
      m3.meta_value as 'phone_number',
      m4.meta_value as 'textarea_position_of_interest',
      m5.meta_value as 'jobs_attachment',
      m6.meta_value as 'job_entry_viewed'
      
      FROM wp_posts p
      LEFT JOIN wp_postmeta m1
          ON p.id = m1.post_id AND m1.meta_key = '_job_posting_entry_contact_name'
      LEFT JOIN wp_postmeta m2
          ON p.id = m2.post_id AND m2.meta_key = '_job_posting_entry_contact_email'
      LEFT JOIN wp_postmeta m3
          ON p.id = m3.post_id AND m3.meta_key = 'phone_phone-number'
      LEFT JOIN wp_postmeta m4
          ON p.id = m4.post_id AND m4.meta_key = 'textarea_position-of-interest'
      LEFT JOIN wp_postmeta m5
          ON p.id = m5.post_id AND m5.meta_key = 'jobs_attachment_input_file_upload-your-resume'
      LEFT JOIN wp_postmeta m6
          ON p.id = m6.post_id AND m6.meta_key = 'job_entry_viewed'
          
        WHERE p.post_type = 'job-entry'";

      $products_data = $wpdb->get_results($sql_data,ARRAY_A);		
      
    //   echo '<pre>';
    //   print_r($duplicates);
    //   echo '</pre>';
    //   exit;

      
      $upload_dir = wp_get_upload_dir();
      $file= $upload_dir['basedir']. '/sfn-data/joblisting_file.csv';

      write_log($file);

      $file = fopen($file, 'w');
      
      // save the column headers
      fputcsv($file, array('Job Title', 'Applicant Name', 'Email', 'Phone Number', 'Position of interest', 'jobs_attachment', 'job_entry_viewed
      '));

            foreach($products_data as $product) {

               // write_log($product);

               $phone_number = unserialize($product['phone_number']);
               $intrest = unserialize($product['textarea_position_of_interest']);
               $jobattach = unserialize($product['jobs_attachment']);
               $jobattach = unserialize($jobattach);

               fputcsv($file, array($product['post_title'], $product['job_applicant_name'], $product['email'],$phone_number['value'], $intrest['value'], $jobattach['value'], $product['job_entry_viewed']));

               

            }

            $file_url = 'https://www.maccosflooring.com/wp-content/uploads/sfn-data/joblisting_file.csv';
            download_url( $file_url, $timeout = 900 );
            $name = 'joblisting_file.csv';
            header('Content-Description: File Transfer');
            header('Content-Type: application/force-download');
            header("Content-Disposition: attachment; filename=\"" . basename($name) . "\";");
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($name));
            ob_clean();
            flush();
            readfile("/var/www/html/wp-content/uploads/sfn-data/".$name); //showing the path to the server where the file is to be download
            exit;


       }

       
  
  //IP location FUnctionality
 if (!wp_next_scheduled('cde_preferred_location_cronjob')) {
    
        wp_schedule_event( time() +  17800, 'daily', 'cde_preferred_location_cronjob');
  }
  
  add_action( 'cde_preferred_location_cronjob', 'cde_preferred_location' );
  
  function cde_preferred_location(){
  
              global $wpdb;
  
              if ( ! function_exists( 'post_exists' ) ) {
                  require_once( ABSPATH . 'wp-admin/includes/post.php' );
              }
  
              //CALL Authentication API:
              $apiObj = new APICaller;
              $inputs = array('grant_type'=>'client_credentials','client_id'=>get_option('CLIENT_CODE'),'client_secret'=>get_option('CLIENTSECRET'));
              $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);
  
  
              if(isset($result['error'])){
                  $msg =$result['error'];                
                  $_SESSION['error'] = $msg;
                  $_SESSION["error_desc"] =$result['error_description'];
                  
              }
              else if(isset($result['access_token'])){
  
                  //API Call for getting website INFO
                  $inputs = array();
                  $headers = array('authorization'=>"bearer ".$result['access_token']);
                  $website = $apiObj->call(BASEURL.get_option('SITE_CODE'),"GET",$inputs,$headers);

                  write_log($website['result']['locations']);
  
                  for($i=0;$i<count($website['result']['locations']);$i++){
  
                      if($website['result']['locations'][$i]['type'] == 'store'){
  
                          $location_name = isset($website['result']['locations'][$i]['city'])?$website['result']['locations'][$i]['city']:"";

                          $found_post = post_exists($location_name,'','','store-locations');
  
                              if( $found_post == 0 ){
  
                                      $array = array(
                                          'post_title' => $location_name,
                                          'post_type' => 'store-locations',
                                          'post_content'  => "",
                                          'post_status'   => 'publish',
                                          'post_author'   => 0,
                                      );
                                      $post_id = wp_insert_post( $array );
                                      
                                      update_post_meta($post_id, 'address', $website['result']['locations'][$i]['address']); 
                                      update_post_meta($post_id, 'city', $website['result']['locations'][$i]['city']); 
                                      update_post_meta($post_id, 'state', $website['result']['locations'][$i]['state']); 
                                      update_post_meta($post_id, 'country', $website['result']['locations'][$i]['country']); 
                                      update_post_meta($post_id, 'postal_code', $website['result']['locations'][$i]['postalCode']); 
                                      if($website['result']['locations'][$i]['forwardingPhone']==''){

                                        update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['phone']);  
                                        
                                      }else{

                                        update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['forwardingPhone']);  
                                      }
                                                                         
                                      update_post_meta($post_id, 'latitude', $website['result']['locations'][$i]['lat']); 
                                      update_post_meta($post_id, 'longitue', $website['result']['locations'][$i]['lng']); 
                                      update_post_meta($post_id, 'monday', $website['result']['locations'][$i]['monday']); 
                                      update_post_meta($post_id, 'tuesday', $website['result']['locations'][$i]['tuesday']); 
                                      update_post_meta($post_id, 'wednesday', $website['result']['locations'][$i]['wednesday']); 
                                      update_post_meta($post_id, 'thursday', $website['result']['locations'][$i]['thursday']); 
                                      update_post_meta($post_id, 'friday', $website['result']['locations'][$i]['friday']); 
                                      update_post_meta($post_id, 'saturday', $website['result']['locations'][$i]['saturday']); 
                                      update_post_meta($post_id, 'sunday', $website['result']['locations'][$i]['sunday']); 
                                      
                              }else{

                                        update_post_meta($found_post, 'address', $website['result']['locations'][$i]['address']); 
                                        update_post_meta($found_post, 'city', $website['result']['locations'][$i]['city']); 
                                        update_post_meta($found_post, 'state', $website['result']['locations'][$i]['state']); 
                                        update_post_meta($found_post, 'country', $website['result']['locations'][$i]['country']); 
                                        update_post_meta($found_post, 'postal_code', $website['result']['locations'][$i]['postalCode']); 
                                        if($website['result']['locations'][$i]['forwardingPhone']==''){

                                        update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['phone']);  
                                        
                                        }else{

                                        update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['forwardingPhone']);  
                                        }
                                                                        
                                        update_post_meta($found_post, 'latitude', $website['result']['locations'][$i]['lat']); 
                                        update_post_meta($found_post, 'longitue', $website['result']['locations'][$i]['lng']); 
                                        update_post_meta($found_post, 'monday', $website['result']['locations'][$i]['monday']); 
                                        update_post_meta($found_post, 'tuesday', $website['result']['locations'][$i]['tuesday']); 
                                        update_post_meta($found_post, 'wednesday', $website['result']['locations'][$i]['wednesday']); 
                                        update_post_meta($found_post, 'thursday', $website['result']['locations'][$i]['thursday']); 
                                        update_post_meta($found_post, 'friday', $website['result']['locations'][$i]['friday']); 
                                        update_post_meta($found_post, 'saturday', $website['result']['locations'][$i]['saturday']); 
                                        update_post_meta($found_post, 'sunday', $website['result']['locations'][$i]['sunday']); 

                              }
  
                      }
                  
                  }
  
              }    
  
  }
  
  
  
  //get ipaddress of visitor
  
  function getUserIpAddr() {
      $ipaddress = '';
      if (isset($_SERVER['HTTP_CLIENT_IP']))
          $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
      else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
          $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
      else if(isset($_SERVER['HTTP_X_FORWARDED']))
          $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
      else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
          $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
      else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
          $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
      else if(isset($_SERVER['HTTP_FORWARDED']))
          $ipaddress = $_SERVER['HTTP_FORWARDED'];
      else if(isset($_SERVER['REMOTE_ADDR']))
          $ipaddress = $_SERVER['REMOTE_ADDR'];
      else
          $ipaddress = 'UNKNOWN';
      
      if ( strstr($ipaddress, ',') ) {
              $tmp = explode(',', $ipaddress,2);
              $ipaddress = trim($tmp[1]);
      }
      return $ipaddress;
  }
  
  
  // Custom function for lat and long
  
  function get_storelisting() {
      
      global $wpdb;
      $content="";
  
  
    //  echo 'no cookie';
      
      $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';
      //12.68.65.195
      $response = wp_remote_post( $urllog, array(
          'method' => 'GET',
          'timeout' => 45,
          'redirection' => 5,
          'httpversion' => '1.0',
          'headers' => array('Content-Type' => 'application/json'),         
          'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
          'blocking' => true,               
          'cookies' => array()
          )
      );
          
          $rawdata = json_decode($response['body'], true);
          $userdata = $rawdata['response'];
  
          $autolat = $userdata['pulseplus-latitude'];
          $autolng = $userdata['pulseplus-longitude'];
          if(isset($_COOKIE['preferred_store'])){
  
              $store_location = $_COOKIE['preferred_store'];
          }else{
  
              $store_location = '';
             
            }
  
          //print_r($rawdata);
          
          $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
                  ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
                  cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
                  sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM wp_posts AS posts
                  INNER JOIN wp_postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'latitude'
                  INNER JOIN wp_postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'longitue'  
                  WHERE posts.post_type = 'store-locations' 
                  AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50000000000000000000 ORDER BY distance";
  
                  write_log($sql);
  
          $storeposts = $wpdb->get_results($sql);
          $storeposts_array = json_decode(json_encode($storeposts), true);      
  
         
          if($store_location ==''){
              //write_log('empty loc');           
              $store_location = $storeposts_array['0']['ID'];
              $store_distance = round($storeposts_array['0']['distance'],1);
          }else{
  
             // write_log('noempty loc');
              $key = array_search($store_location, array_column($storeposts_array, 'ID'));
              $store_distance = round($storeposts_array[$key]['distance'],1);           
  
          }
          
          $content .= '<div class="header_store"><div class="store_wrapper">';
          $content .='<div class="locationWrapFlyer">
          
          <div class="contentFlyer"><div> 
              <p>You\'re Store</p>';
          $content .= '<h3 class="title-prefix">'.get_the_title($store_location).'</h3>';        
          $content .= '<h5 class="title-prefix" style="font-style: italic;font-size:12px;"><a href="javascript:void(0)" target="_self" id="openFlyer" class="store-cta-link headeropenFlyer">View All Locations</a></h5>';
          $content .= '</div></div></div>';
          
         
  
      $content_list = '<div id="storeLocation" class="changeLocation">';
      $content_list .= '<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>';
      $content_list .= '<div class="content">'; 
    
      foreach ( $storeposts as $post ) {

        write_log(get_field('phone', $post->ID));
       
          $content_list .= '<div class="store_wrapper" id ="'.$post->ID.'">';
          $content_list .= '<h5 class="title-prefix">'.get_the_title($post->ID).'  - <b>'.round($post->distance,1).' MI</b></h5>';
          $content_list .= '<h5 class="store-add">'.get_field('address', $post->ID).' '.get_field('city',$post->ID).', '.get_field('state',$post->ID).' '.get_field('postal_code', $post->ID).'</h5>';
          
          if(get_field('closed', $post->ID)){

              $content_list .= '<p class="store-hour redtext">'.get_field('hours', $post->ID).'</p>';
              $content_list .= '<p>Still happly serving this area in the meanwhile at our Humble location. Please call for details</p>';
              
          }else{
             
                      $day = get_field(strtolower(date("l")), $post->ID);                     

                      if($day == 'Closed'){

                        $content_list .= '<p class="store-hour">CLOSED TODAY</p>';

                      }else{
                        $daystr = strval($day);
                        $closedtime = explode("–",$daystr);                        
                        $content_list .= '<p class="store-hour">OPEN UNTIL '.trim($closedtime[1]).'</p>';
                      }
                    
              
          }
          $content_list .= '<p class="store-hour"><span class="phoneIcon"></span> &nbsp;'.get_field('phone', $post->ID).'</p>';
          $content_list .= '<a href="javascript:void(0)" data-id="'.$post->ID.'" data-storename="'.get_the_title($post->ID).'" data-distance="'.round($post->distance,1).'" target="_self" class="store-cta-link choose_location">Choose This Location</a>';
          if(get_field('store_link',$post->ID)){
              $content_list .= '<br><a href="'.get_field('store_link',$post->ID).'" target="_self" class="store-cta-link view_location"> View Location</a>';
          }
          $content_list .= '</div>'; 
      }
      
      $content_list .= '</div>';
      $content_list .= '</div>';
  
      $data = array();
  
      $data['header']= $content;
      $data['list']= $content_list;

      write_log($data['list']);
  
      echo json_encode($data);
          wp_die();
  }
  
  //add_shortcode('storelisting', 'get_storelisting');
  add_action( 'wp_ajax_nopriv_get_storelisting', 'get_storelisting' );
  add_action( 'wp_ajax_get_storelisting', 'get_storelisting' );
  
  
  
  //choose this location FUnctionality
  
  add_action( 'wp_ajax_nopriv_choose_location', 'choose_location' );
  add_action( 'wp_ajax_choose_location', 'choose_location' );
  
  function choose_location() {
     
          $content = '<div class="store_wrapper">';
          $content .='<div class="locationWrapFlyer">
         
          <div class="contentFlyer"><div>
              <p>You\'re Store</p>';
  
          $content .= '<h3 class="title-prefix">'. get_the_title($_POST['store_id']) .'</h3>';
          
         $content .= '<h5 class="title-prefix" style="font-style: italic;font-size:12px;"><a href="javascript:void(0)" target="_self" id="openFlyer" class="store-cta-link headeropenFlyer">View All Locations</a></h5>';
          $content .= '</div></div>';
  
          echo $content;
  
      wp_die();
  }
  
//   function my_after_header() {
//       echo '<div class="header_store"></div><div id="ajaxstorelisting"> </div>
//       <div class="locationOverlay"></div>';
//     }
//     add_action( 'fl_after_header', 'my_after_header' );

 function location_header() {
        return '<div class="header_store"></div><div id="ajaxstorelisting"> </div>
        <div class="locationOverlay"></div>';
      }
add_shortcode('location_code', 'location_header');

add_filter( 'gform_pre_render_9', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_9', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_9', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_9', 'populate_product_location_form' );

add_filter( 'gform_pre_render_17', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_17', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_17', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_17', 'populate_product_location_form' );

add_filter( 'gform_pre_render_11', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_11', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_11', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_11', 'populate_product_location_form' );

add_filter( 'gform_pre_render_18', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_18', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_18', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_18', 'populate_product_location_form' );

function populate_product_location_form( $form ) {

    foreach ( $form['fields'] as &$field ) {

        // Only populate field ID 12
        if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-store' ) === false ) {
            continue;
        }      	

            $args = array(
                'post_type'      => 'store-locations',
                'posts_per_page' => -1,
                'post_status'    => 'publish'
            );										
          
             $locations =  get_posts( $args );

             $choices = array(); 

             foreach($locations as $location) {
                

                  $title = get_the_title($location->ID);
                  
                       $choices[] = array( 'text' => $title, 'value' => $title );

              }
              wp_reset_postdata();

             // write_log($choices);

             // Set placeholder text for dropdown
             $field->placeholder = '-- Choose Location --';

             // Set choices from array of ACF values
             $field->choices = $choices;
        
   }
return $form;

}
		 //Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_override_yoast_breadcrumb_trail',90 );

function wpse_override_yoast_breadcrumb_trail( $links ) {

    if (is_singular( 'carpeting' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/carpeting/',
            'text' => 'Carpet',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/carpeting/carpeting-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
	else  if (is_singular( 'hardwood_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/hardwood-flooring/',
            'text' => 'Hardwood',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/hardwood-flooring/hardwood-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
	else  if (is_singular( 'laminate_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/laminate-flooring/',
            'text' => 'Laminate',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/laminate-flooring/laminate-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
	else  if (is_singular( 'tile_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/tile/',
            'text' => 'Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/tile/tile-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
	else  if (is_singular( 'luxury_vinyl_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/luxury-vinyl-tile/',
            'text' => 'Luxury Vinyl',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/luxury-vinyl-tile/vinyl-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
	
    
    return $links;
}

//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
   // write_log($sql_delete);	
}
		add_filter( 'auto_update_plugin', '__return_false' );